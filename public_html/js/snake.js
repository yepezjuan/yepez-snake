/*-----------------------------------------------------------------------
 * Variables
 * ----------------------------------------------------------------
 */


var snake;
var snakeLength;
var snakeSize;
var snakeDirection;
var remainder;


var food;

var context;
var screenWidth;
var screenHeight;

var image;

var gameState;
var gameOverMenu;
var Menu;
var restartbutton;
var playHUD;
var scoreboard;

var levelsMenu;

var easyButton;
var mediumButton;
var hardButton;
var blopSound;
var glassSound;

var highscore;
/*======================================================================
 * exicuting game codes
 * $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
 */

gameInitialize();
snakeInitialize();
foodInitialize();
setInterval(gameLoop, 1000 / 30);

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++9897966443442-00878
 * Game FunctionS
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=099077 
 */


function gameInitialize() {
    var canvas = document.getElementById("game-screen");
    context = canvas.getContext("2d");

    screenWidth = window.innerWidth - 30;
    screenHeight = window.innerHeight - 30;


    canvas.width = screenWidth;
    canvas.height = screenHeight;

    document.addEventListener("keydown", keyboardHandler);

    gameOverMenu = document.getElementById("gameOver");
    centerMenuPosition(gameOverMenu);

    restartbutton = document.getElementById("restartbutton");
    restartbutton.addEventListener("click", gameRestart);

    Menu = document.getElementById("menuDisplay");

    playButton = document.getElementById("playButton");
    playButton.addEventListener("click", gameStart);

    playHUD = document.getElementById("playHUD");
    scoreboard = document.getElementById("scoreboard");

    levelsMenu = document.getElementById("levelDisplay");
    centerMenuPosition(levelsMenu);

    easyButton = document.getElementById("easy");
    easyButton.addEventListener("click", gameStartEasy);

    mediumButton = document.getElementById("medium");
    mediumButton.addEventListener("click", gameStartMedium);

    hardButton = document.getElementById("hard");
    hardButton.addEventListener("click", gameStartHard);

    blopSound = new Audio("sounds/brakefast");
    blopSound.preload = "auto";

    glassSound = new Audio("sounds/glass-break.mp3");
    glassSound.preload = "auto";

    image = document.getElementById("source");
    



    setState("MENU");
}

function gameLoop() {
    gameDraw();
    drawscoreboard();
    if (gameState == "PLAY") {
        snakeUpdate();
        snakeDraw();
        foodDraw();
    }

}

function gameDraw() {
    context.fillStyle = "rgb(48, 104, 122)";
    context.fillRect(0, 0, screenWidth, screenHeight);

}

function gameRestart() {
    snakeInitialize();
    foodInitialize();
    hideMenu(gameOverMenu);
    setState("MENU");
}

function gameStart() {
    snakeInitialize();
    foodInitialize();
    hideMenu(Menu);
    setState("LEVELS");
}
function gameStartEasy() {
    snakeInitialize();
    foodInitialize();
    hideMenu(levelsMenu);
    setState("PLAY");
    setInterval(gameLoop, 2000 / 10);
}

function gameStartMedium() {
    snakeInitialize();
    foodInitialize();
    hideMenu(levelsMenu);
    setState("PLAY");
    setInterval(gameLoop, 5000 / 20);
}

function gameStartHard() {
    snakeInitialize();
    foodInitialize();
    hideMenu(levelsMenu);
    setState("PLAY");
    setInterval(gameLoop, 900 / 30);
}

/***************************************************************************
 * Snake Function
 * *************************************************************************
 */

function snakeInitialize() {
    snake = [];
    snakeLength = 1;
    snakeSize = 25;
    snakeDirection = "down";

    for (var index = snakeLength - 1; index >= 0; index--) {
        snake.push({
            x: index,
            y: 0
        });
    }
}

function snakeDraw() {
    for (var index = 0; index < snake.length; index++) {

        var remainder = index % 2;
        if (remainder === 0) {
            context.fillStyle = "black";
        }
        else {
            context.fillStyle = "#FF7700";
        }

        context.strokeStyle = "#009900";
        context.strokeRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);

        context.fillRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
        context.lineWidth = 10;

    }
}
function snakeUpdate() {
    var snakeHeadX = snake[0].x;
    var snakeHeadY = snake[0].y;

    if (snakeDirection == "down") {
        snakeHeadY++;
    }
    else if (snakeDirection == "right") {
        snakeHeadX++;
    }

    if (snakeDirection == "up") {
        snakeHeadY--;
    }

    else if (snakeDirection == "left") {
        snakeHeadX--;
    }

    checkFoodCollisions(snakeHeadX, snakeHeadY);
    checkWallCollisions(snakeHeadX, snakeHeadY);
    checkSnakeCollisions(snakeHeadX, snakeHeadY);

    var snakeTail = snake.pop();
    snakeTail.x = snakeHeadX;
    snakeTail.y = snakeHeadY;
    snake.unshift(snakeTail);

}

/************************************************************************
 * food functions
 *************************************************************************/

function foodInitialize() {
    food = {
        x: 0,
        y: 0

    };
    setFoodPosition();
}
function foodDraw() {
    context.drawImage(image, food.x * snakeSize, food.y * snakeSize, snakeSize, snakeSize);

}

function setFoodPosition() {
    var randomX = Math.floor(Math.random() * screenWidth);
    var randomY = Math.floor(Math.random() * screenHeight);

    food.x = Math.floor(randomX / snakeSize);
    food.y = Math.floor(randomY / snakeSize);
}
/***********************************************************************
 *
 ***********************************************************************  
 */
function keyboardHandler(event) {
    console.log(event);

    if (event.keyCode == "39" && snakeDirection != "left") {
        snakeDirection = "right";
    }

    else if (event.keyCode == "40" && snakeDirection != "up") {
        snakeDirection = "down";
    }

    if (event.keyCode == "37"  && snakeDirection != "right") {
        snakeDirection = "left";
    }

    else if (event.keyCode == "38" && snakeDirection != "down") {
        snakeDirection = "up";
    }

}

/****************************************************************************
 * collision handling
 * ****************************************************************************
 */

function checkFoodCollisions(snakeHeadX, snakeHeadY) {
    if (snakeHeadX == food.x && snakeHeadY == food.y) {
        snake.push({
        });
        snake.push({
        });
        snake.push({
        });
        snake.push({
        });

        snakeLength++;
        snakeLength++;
        snakeLength++;
        snakeLength++;
        setFoodPosition();
        blopSound.play();
    }

}

function checkWallCollisions(snakeHeadX, snakeHeadY) {
    if (snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0) {
        setState("GAME OVER");
        glassSound.play();
    }
    else if (snakeHeadY * snakeSize >= screenHeight || snakeHeadY * snakeSize < 0) {
        setState("GAME OVER");
        glassSound.play();
    }

}

function checkSnakeCollisions(snakeHeadX, snakeHeadY) {
    for (var index = 1; index < snake.length; index++) {
        if (snakeHeadX == snake[index].x && snakeHeadY == snake[index].y) {
            setState("GAME OVER");
            return;
        }
    }
}

/****----------------------------------------------------------------------
 * game state handling
 * ------------------------------------------------------------------------
 */

function setState(state) {
    gameState = state;
    showMenu(state);
}
/*----------------------------------------------------------------
 * menu functions 
 * ----------------------------------------------------------------
 */

function displayMenu(menu) {
    menu.style.visibility = "visible";
}

function hideMenu(menu) {
    menu.style.visibility = "hidden";
}


function showMenu(state) {
    if (state == "GAME OVER") {
        displayMenu(gameOverMenu);
    }

    else if (state == "PLAY") {
        displayMenu(playHUD);
    }
    else if (state == "MENU") {
        displayMenu(Menu);
    }
    else if (state == "LEVELS") {
        displayMenu(levelsMenu);
    }
}

function centerMenuPosition(menu) {
    menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
    menu.style.left = (screenWidth / 2) - (menu.offsetWidth / 2) + "px";
}

function drawscoreboard() {
    scoreboard.innerHTML = "Length: " + snakeLength;
}